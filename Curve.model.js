import { Coordinate } from "./Coordinate.model";

export class Curve {
    constructor (ip, cp0, cp1, fp) {
        this.ip = ip; // Initial Point
        this.cp0 = cp0; // Control Point 1
        this.cp1 = cp1; // Control Point 2
        this.fp = fp; // Final Point
    }

    get svgPath() {
        return `M${this.ip.toString()} C${this.cp0.toString()} ${this.cp1.toString()} ${this.fp.toString()}`;
    }

    set finalPoint(fp) {
        this.fp = fp;
    }

    /*
     * @param m: Number of points to place on curve
     * @return array with equidistant points
     */
    getEquidistantPoints(m) {
        const n = 100;
        const s = 1 / (n - 1);
        const increments = [];
        const pointsInCurve = [];
        const distances = [];

        for (let i = 0; i < n; i++) {
            increments[i] = i*s;
            pointsInCurve[i] = this.getCurveLocation(increments[i]);
            if (i > 0) {
                distances[i] = Coordinate.distance(pointsInCurve[i], pointsInCurve[i-1]);
            }
        }
        // Get fractional arclengths along polyline
        const dd = [0];
        for (let i = 1 ; i < n ; i++) {
            // Accumulated distances
            dd[i] = dd[i-1] + distances[i];
        }
        for (let i = 1 ; i < n ; i++) {
            dd[i] = dd[i] / dd[n-1];
        }

        const step = 1 / (m + 1);
        const result = [];

        // values at 0 and m+1 are disregarded
        for (let r = 1; r <= m; r++) {
            const d = r*step;
            const i = findIndex(dd, d);
            const u = (d - dd[i]) / (dd[i+1] - dd[i]);
            const t = (i + u) * s;
            result[r] = this.getCurveLocation(t);
        }
        return result;
    }

    getCurveLocation(t) {
        /*
         * Cubic bezier ecuation
         * B(t) = (1 - t)^3.P0 + 3(1 - t)^2.t.P1 + 3(1 - t).t^2.P2 + t^3.P3
         */
        return Coordinate.sum(
            Coordinate.multiply(Math.pow(1 - t, 3), this.ip),
            Coordinate.multiply(3 * Math.pow(1 - t, 2) * t, this.cp0),
            Coordinate.multiply(3 * (1 - t) * Math.pow(t, 2), this.cp1),
            Coordinate.multiply(Math.pow(t, 3), this.fp)
        );
    }
}

/*
 * Find index i such that dd[i] < d < dd[i+1]
 */
function findIndex(dd, d) {
    let i = 0;
    for (let j = 0 ; j < dd.length ; j++)
    {
        if (d > dd[j]) {
            i = j;
        }
    }
    return i;
}

