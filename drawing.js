const SVG_NAMESPACE = 'http://www.w3.org/2000/svg';
const svgCanvas = document.getElementById('canvas');

export function removeCurve(group) {
    svgCanvas.removeChild(group);
}

export function redrawElements(item, elementsCount) {
    const { curve, svgGroup } = item;
    const circles = svgGroup.querySelectorAll('circle');
    Array.from(circles).forEach(circle => svgGroup.removeChild(circle))

    drawElements(svgGroup, curve, elementsCount);
}

export function redrawCurve(item, elementsCount) {
    svgCanvas.removeChild(item.svgGroup);
    drawCurve(item, elementsCount);
}

export function drawCurves(iterable, elementsCount) {
    for (let item of iterable) {
        drawCurve(item, elementsCount);
    }
}

function drawCurve(item, elementsCount) {
    const svgGroup = document.createElementNS(SVG_NAMESPACE, 'g');
    const svgPath = document.createElementNS(SVG_NAMESPACE, 'path');
    svgCanvas.appendChild(svgGroup);
    svgGroup.appendChild(svgPath);
    svgPath.setAttribute('d', item.curve.svgPath);

    const elements = drawElements(svgGroup, item.curve, elementsCount);
    item.svgGroup = svgGroup;
}

function drawElement(svgCanvas, curve, t) {
    const circle = document.createElementNS(SVG_NAMESPACE, 'circle');
    circle.setAttribute('cx', t.x);
    circle.setAttribute('cy', t.y);
    circle.setAttribute('r', 3);
    svgCanvas.appendChild(circle);
    return circle;
}

function drawElements(svgCanvas, curve, n) {
    const elementsT = curve.getEquidistantPoints(n);
    return elementsT.map(t => drawElement(svgCanvas, curve, t));
}

function getElements(n) {
    return Array.from({length: n}, (value, key) => (key + 1) / (n + 1))
}

