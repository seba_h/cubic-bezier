const params = {
    curvesCount: 0,
    elementsCount: 5,
    maxX: 0,
    maxY: 0
};

export class Params {
    get curvesCount() {
        return params.curvesCount;
    }

    set curvesCount(value) {
        params.curvesCount = value;
    }

    get elementsCount() {
        return params.elementsCount;
    }

    set elementsCount(value) {
        params.elementsCount = value;
    }

    get maxX() {
        return params.maxX;
    }

    set maxX(value) {
        params.maxX = value;
    }

    get maxY() {
        return params.maxY;
    }

    set maxY(value) {
        params.maxY = value;
    }
}
