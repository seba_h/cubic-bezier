export class Coordinate {
    constructor(x, y) {
        this.x = x,
        this.y = y
    }
    toString() {
        return `${this.x},${this.y}`;
    }

    static multiply(n, p) {
        return new Coordinate(p.x * n, p.y * n);
    }

    static sum(...points) {
        return points.reduce((acc, cur) => {
            acc.x += cur.x;
            acc.y += cur.y;
            return acc;
        }, { x: 0, y: 0 });
    }

    static distance(p0, p1) {
        return Math.hypot(p1.x - p0.x, p1.y - p0.y);
    }
}
