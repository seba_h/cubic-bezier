# To run the project
1. Install dependencies: ```yarn```
2. Run the project: ```yarn start```

## Resizing
When the browser is resized, the final point of every curve is set again, keeping the remaining control points untouched. If the new size of the screen is smaller, some of the control points could fall outside of the screen.

## Data structure
|Property   |Description|
|-----------|-----------|
| curve     | Model     |
| svgGroup  | SVG group containing the bezier curve `path` and all the elements (represented by a `circle`)     |

## File structure
```
src
├── main.js: DOM events
├── drawing.js: SVG handling
├── model.js: in-memory representation of the curves
├── Coordinate.model.js
├── Curve.model.js
└── params.js: class to store app parameters
```

## SVG structure
Bezier curves representation:
```xml
<g>
    <path ... />
    <circle ... />
    <circle ... />
    <circle ... />
</g>
```

## Tested in Chrome
