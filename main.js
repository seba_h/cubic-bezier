import 'babel-polyfill';
import * as drawing from './drawing';
import * as model from './model';

updateWindowSize();
setEventHandlers();
setCurvesCount(5);

// Event handlers
function setEventHandlers() {
    document.getElementById('txtCurves').addEventListener('change', handleSetCurvesCount);
    document.getElementById('txtElements').addEventListener('change', handleSetElementsCount);
    window.addEventListener('resize', handleResize);
}

function handleResize() {
    updateWindowSize();
    const { getItems, params, updateFinalControlPoint } = model;
    for (let item of getItems()) {
        updateFinalControlPoint(item);
        drawing.redrawCurve(item, params.elementsCount);
    }
}

function handleSetCurvesCount(event) {
    const count = +event.srcElement.value;
    setCurvesCount(count);
    document.getElementById('curvesCount').innerText = count;
}

function handleSetElementsCount(event) {
    const count = +event.srcElement.value;
    setElementsCount(count);
    document.getElementById('elementsCount').innerText = count;
}

function updateWindowSize() {
    const doc = document.documentElement;
    const maxX = Math.max(doc.clientWidth, window.innerWidth || 0) - 20; // Window's width
    const maxY = Math.max(doc.clientHeight, window.innerHeight || 0) - 20; // Window's height
    model.params.maxX = maxX;
    model.params.maxY = maxY;
}

// Helper functions
function setCurvesCount(count) {
    model.params.curvesCount = count;
    const diff = count - model.getItemsCount();
    if (diff > 0) {
        for (let i = 0; i < diff; i++) {
            model.addNewCurve();
        }
        const from = model.getItemsCount() - diff;
        drawing.drawCurves(model.getItems(from), model.params.elementsCount);
    }
    else {
        const iterator = model.removeCurves(count);
        for (let curve of iterator) {
            drawing.removeCurve(curve.svgGroup);
        }
    }
}

function setElementsCount(count) {
    model.params.elementsCount = count;
    for (let item of model.getItems()) {
        drawing.redrawElements(item, model.params.elementsCount);
    }
}

