import { Coordinate } from "./Coordinate.model";
import { Curve } from "./Curve.model";
import { Params } from './params';

export const params = new Params();
const curves = [];

export function getItemsCount() {
    return curves.length;
}

export function *removeCurves(count) {
    while (curves.length > count) {
        const item = curves.pop();
        yield item;
    }
}

export function addNewCurve() {
    curves.push({ curve: createCurve() });
}

export function updateFinalControlPoint(item) {
    item.curve.finalPoint = new Coordinate(params.maxX, params.maxY);
}

export function *getItems(from = 0) {
    for (let i = from ; i < curves.length; i++) {
        yield curves[i];
    }
}

export function createCurve() {
    const Ip = new Coordinate(0,0);
    const Cp0 = new Coordinate(params.maxX * Math.random(), params.maxY * Math.random());
    const Cp1 = new Coordinate(params.maxX * Math.random(), params.maxY * Math.random());
    const Fp = new Coordinate(params.maxX, params.maxY);

    return new Curve(Ip, Cp0, Cp1, Fp);
}
